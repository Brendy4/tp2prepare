from bottle import route, run, template
from os import environ


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/')
def hello():
    return 'hello wold'


run(host='0.0.0.0', port=environ.get('PORT', 8080))
